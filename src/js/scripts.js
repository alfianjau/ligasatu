(function ($, window, document, undefined) {
  "use strict";

  //global
  ! function () {

    function n(n) {
      var e = "";
      return e += '<button type="button" data-role="none" class="slick-' + t, e += ' aria-label="Previous" tabindex="0" role="button">', e += n, e += "</button>"
    }
    var e = {
      getSliderBtn: n
    };
    window.Helper = e
  }();

  // select 2 js
  function selectHelper() {
    jQuery(".select").select2({ width: "100%" });
  }

  // custom function initiation
  var isActive = "is-active";
  function progressMatch() {
    var strHome = jQuery(".result--home").text();
    var strAway = jQuery(".result--away").text();
    var home = parseInt(strHome);
    var away = parseInt(strAway);
    var result = home + away;
    jQuery(".sr-only").text(result);
  }

  function matchInfoToggle() {
    var e = jQuery(".match-infos");
    e.length && e.on("click", ".match-info", function (e) {
      var t = jQuery(e.currentTarget);
      t.hasClass(isActive) || (t.siblings(".match-info").removeClass(isActive), t.addClass(isActive))
    })
  }

  function dropdownEnabler() {
    jQuery('.dropdown-toggle').click(function () {
      var location = jQuery(this).attr('href');
      window.location.href = location;
      return false;
    });
  }

  function tableClicker() {
    jQuery(".clickable-row").click(function () {
      // e.preventDefault();
      window.location = jQuery(this).data("href");
    });
  }

  function buttonUp() {
    var valux = jQuery('.sb-search__input').val();
    valux = jQuery.trim(valux).length;
    if (valux !== 0) {
      jQuery('.sb-search__submit').css('z-index', '99');
    } else {
      jQuery('.sb-search__input').val('');
      jQuery('.sb-search__submit').css('z-index', '-999');
    }
  }

  function lazy() {
    jQuery(".lazy").lazyload();
  }

  function sbSearch() {
    var submitIcon = jQuery('.sb-icon__search');
    var submitInput = jQuery('.sb-search__input');
    var searchBox = jQuery('.sb-search');
    var isOpen = false;

    jQuery(document).mouseup(function () {
      if (isOpen == true) {
        submitInput.val('');
        jQuery('.sb-search__submit').css('z-index', '-999');
        submitIcon.click();
      }
    });

    submitIcon.mouseup(function () {
      return false;
    });

    searchBox.mouseup(function () {
      return false;
    });

    submitIcon.click(function () {
      if (isOpen == false) {
        searchBox.addClass('sb-search-open');
        isOpen = true;
      } else {
        searchBox.removeClass('sb-search-open');
        isOpen = false;
      }
    });
  }

  function featherVideo() {
    jQuery(".featherlight").featherlight({});
  }

  function getSliderBtn(direction, content) {
    var btn = '';
    btn += '<button type="button" data-role="none" class="slick-' + direction;
    btn += ' aria-label="Previous" tabindex="0" role="button">';
    btn += content;
    btn += '</button>';
    return btn;
  }

  function matchRotator() {
    jQuery('.match-schedule-rotators').slick({
      centerMode: !0,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: !1,
      focusOnSelect: !1,
      useTransform: false,
      prevArrow: getSliderBtn('prev', '<span class="fa fa-fw fa-angle-left">'),
      nextArrow: getSliderBtn('next', '<span class="fa fa-fw fa-angle-right">'),
      mobileFirst: true,
      variableWidth: !0,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            variableWidth: true
          }
        }
      ]
    });

    var $scheduleSlider = jQuery('.match-schedule-rotators');
    var $scheduleRotators = jQuery('.match-schedule-rotator');
    var $container = jQuery('#matchInfoContainer');
    var templateUrl = $scheduleSlider.attr('data-template');
    var len = $scheduleRotators.length - 1;
    var $nextArrow = $scheduleSlider.find('.slick-next');
    var $placeholder = $scheduleSlider.find('.is-placeholder');
    var isLoading = 'is-loading';

    jQuery.ajax({ url: templateUrl }).done(function (result) {
      var TEMPLATE = Handlebars.compile(result);

      $scheduleRotators.on('click', function (e) {
        var index = e.currentTarget.getAttribute('data-index');
        $scheduleSlider.slick('slickGoTo', index);
      });

      $scheduleSlider.on('beforeChange', function (e) {
        $scheduleSlider.addClass(isLoading);
        $container.addClass(isLoading);
      });

      $scheduleSlider.on('afterChange', function (slick, currentSlide) {
        var $currentItem = $scheduleSlider.find('.slick-current .match-schedule-rotator');
        var infoUrl = $currentItem.attr('data-info');

        jQuery.getJSON(infoUrl, function (respond, status) {
          var output = TEMPLATE(respond);
          var currentIndex = Number($currentItem.attr('data-index'));

          $container.empty().append(output);
          $container.removeClass(isLoading);
          $scheduleSlider.removeClass(isLoading);

          if ($placeholder.length) {
            if (currentIndex + 1 === len) {
              $nextArrow.prop('disabled', 'disabled');
            } else {
              $nextArrow.removeAttr('disabled');
            }
          }
        });
      });
    });
  }

  function pagination() {
    // urung
  }

  function galleryCarousel() {
    jQuery(".article-test-head").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".article-tes",
      autoplay: false
    });
    jQuery(".article-tes").slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: ".article-test-head",
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      arrows: false

    });
  }

  function homeSlider() {
    jQuery(".home-slider").slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      fade: false,
      cssEase: "linear",
      autoplay: true,
      autoplaySpeed: 2000
    });
  }

  function pertandinganSlider() {
    jQuery(".column--jadwal__wrap").slick({
      arrows: true,
      speed: 500,
      fade: false,
      cssEase: "linear",
      adaptiveHeight: true
    });
  }

  function reveal() {
    // reveal
    window.sr = ScrollReveal({ reset: false });
    // sr.reveal(".column");
    sr.reveal(".card");
    sr.reveal(".stats");
    // end reveal
  }

  function tilt() {
    // tilting
    jQuery(".stats").tilt();
    // end tilting
  }

  // end function initiation

  jQuery(function () {
    // custom function execution
    galleryCarousel();
    selectHelper();
    reveal();
    tilt();
    buttonUp();
    sbSearch();
    dropdownEnabler();
    homeSlider();
    pertandinganSlider();
    pagination();
    progressMatch();
    matchInfoToggle();
    matchRotator();
    tableClicker();

    // lazy();
    // end function execution

    // end function execution

  });
})(jQuery, window, document);
